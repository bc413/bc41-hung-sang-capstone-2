let renderProductsList = (array) => {
    var contentHTML = "";
    array.forEach(function (product) {
      var contentTr = `<div class="card p-3" style="width: 30%; margin: 0 1.5% 1.5% 1.5%;">
                          <img class="card-img-top" src="${product.img}" alt="Card image">
                          <div class="card-body">
                              <h4 class="card-title">${product.name}</h4>
                              <p class="card-text">Giá: ${product.price} $.</p>
                              <p class="card-text">Màn hình: ${product.screen}.</p>
                              <p class="card-text">Camera sau: ${product.backCamera}.</p>
                              <p class="card-text">Camera trước: ${product.frontCamera}.</p>
                              <p class="card-text">Mô tả: ${product.desc}.</p>
                          </div>
                          <div class="card-footer bg-transparent border-0">
                              <button onclick="addToCart(${product.id})" type="" class="btn btn-primary"><i class="fa fa-cart-plus"></i></button>
                          </div>
                      </div>`;
      contentHTML += contentTr;
    });
  
    document.getElementById("products-list").innerHTML = `${contentHTML}`;
  };
  
  // Filter
  const select = document.getElementById("product-type-filter");
  select.addEventListener("change", function () {
    const selectedType = this.value;
    let filteredProducts = [];
    if (selectedType === "All") {
      filteredProducts = ProductsList;
    } else {
      for (let i = 0; i < ProductsList.length; i++) {
        if (ProductsList[i].type === selectedType) {
          filteredProducts.push(ProductsList[i]);
        }
      }
    }
    renderProductsList(filteredProducts);
  });
  
  // Render cart
  let renderCart = (array) => {
    let cartList = document.querySelector("#cart-items");
    let html = "";
  
    for (let i = 0; i < array.length; i++) {
      let product = array[i];
      html += `
        <tr>
          <td class="text-black">${product.name}</td>
          <td class="text-black">$${product.price}</td>
          <td class="text-black">${product.quantity}</td>
          <td>
            <button onclick="increaseQty(${product.id})">
              <i class="fa fa-plus"></i>
            </button>
          </td>
          <td>
            <button onclick="decreaseQty(${product.id})">
              <i class="fa fa-minus"></i>
            </button>
          </td><td>
          <button onclick="remove(${product.id})">
              <i class="fa fa-trash"></i>
          </button>
        </td>
        </tr>
        <br />
      `;
    }
  
    cartList.innerHTML = html;
  
    let totalPrice = array.reduce((acc, item) => {
      return acc + item.quantity * item.price;
    }, 0);
    document.getElementById("total-price").innerHTML = totalPrice;
  };
  
  document.getElementById("shopping-cart").addEventListener("click", function () {
    var cover = document.getElementById("cover");
    if (cover.style.display === "none") {
      cover.style.display = "block";
    } else {
      cover.style.display = "none";
    }
  });
  