let BASE_URL = `https://63da51d8b28a3148f684171b.mockapi.io`;
let ProductsList = [];
let Cart = [];
let CartJSON = localStorage.getItem("CART_LOCAL");
// console.log(CartJSON);
if (CartJSON != null) {
  Cart = JSON.parse(localStorage.getItem("CART_LOCAL")) || [];
  console.log("Cart :", Cart);
}
let fetchProductsList = () => {
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then(function (res) {
      ProductsList = res.data;
      renderProductsList(ProductsList);
      console.log("ProductsList :", ProductsList);
    })
    .catch(function (err) {
      console.log("err:", err);
    });
};
fetchProductsList();
renderCart(Cart);
let addToCart = (productId) => {
  let productIndex = Cart.findIndex((product) => product.id == productId);
  if (productIndex == -1) {
    let product = ProductsList.find((product) => product.id == productId);
    Cart.push({ ...product, quantity: 1 });
  } else {
    Cart[productIndex].quantity += 1;
  }
  let CartJSON = JSON.stringify(Cart);
  localStorage.setItem("CART_LOCAL", CartJSON);
  renderCart(Cart);
  console.log(Cart);
};

let increaseQty = (productId) => {
  const itemIndex = Cart.findIndex((item) => item.id == productId);
  if (itemIndex != -1) {
    Cart[itemIndex].quantity += 1;
    let CartJSON = JSON.stringify(Cart);
    localStorage.setItem("CART_LOCAL", CartJSON);
    renderCart(Cart);
    console.log(Cart);
  }
};

let decreaseQty = (productId) => {
  const itemIndex = Cart.findIndex((item) => item.id == productId);
  if (itemIndex != -1) {
    Cart[itemIndex].quantity -= 1;
    if (Cart[itemIndex].quantity <= 0) {
      Cart.splice(itemIndex, 1);
    }
    let CartJSON = JSON.stringify(Cart);
    localStorage.setItem("CART_LOCAL", CartJSON);
    renderCart(Cart);
    console.log(Cart);
  }
};

let clearCart = () => {
  Cart = [];
  let CartJSON = JSON.stringify(Cart);
  localStorage.setItem("CART_LOCAL", CartJSON);
  renderCart(Cart);
};

let remove = (productId) => {
  Cart = Cart.filter((item) => item.id != productId);
  let CartJSON = JSON.stringify(Cart);
  localStorage.setItem("CART_LOCAL", CartJSON);
  renderCart(Cart);
};
