let thongBao = ["Hãy nhập Mã", "Hãy nhập Tên","Hãy nhập Giá","Hãy nhập Hình Ảnh","Hãy viết mô tả","Hãy nhập kích thước màn hình", "Nhập thông số camera trước","Nhập thông số camera sau", "Chọn loại sản phẩm"];

export let kietTraNhap = (value,spanTB,index) =>{
    let maSp = document.querySelector(value).value;
    if(maSp == ""){
        document.querySelector(spanTB).innerHTML = thongBao[index];
        return false;
    }else{
        document.querySelector(spanTB).style.display = "none"
        return true;
    }
}

let thongBaoNhap = ["Chỉ được nhập số","Chỉ được nhập chữ"]

export let kiemTraSo = (value,spanTB,index) =>{
    let nhapSo = document.querySelector(value).value
    let number = /^[0-9]+$/;
    if(nhapSo.match(number)){
        document.querySelector(spanTB).style.display = "none"
        return true;
    }else{
        document.querySelector(spanTB).style.display = "block"
        document.querySelector(spanTB).innerHTML =  thongBaoNhap[index];
        return false;
    }
}

// export let kiemTraChu = (value,spanTB,index) =>{
//     let nhapChu = document.querySelector(value).value
//     let letters = new RegExp ("^[A-Za-z]+$");
//     if(letters.test(nhapChu)){
//         document.querySelector(spanTB).style.display = "none";
//         return true;
//     }else{
//         document.querySelector(spanTB).style.display = "block";
//         document.querySelector(spanTB).innerHTML =  thongBaoNhap[index];
//         return false;
//     }
// }   


export let kiemTraTrung = (url,tenSanPham) =>{
    axios({
        url: url,
        method: 'GET',
    }) 
    .then((ress)=>{
        let data = ress.data;
        let viTri = data.findIndex((item)=>{
            return item.name == tenSanPham;
        })
        if(viTri == -1){
            document.querySelector("#tbTen").style.display = "none"
            console.log("Ok")
            return true;
        }
        else{
            document.querySelector("#tbTen").style.display = "block"
            document.querySelector("#tbTen").innerHTML = `Tên bị trùng`
            console.log("Bi trung")
            return false;
        }
    })
    .catch((err)=>{
        console.log(err)
    });
    return true;
};


export let checkDoDdai =(value, min, max, showSpan, text) => {
    if(value.length < min || value.length > max ){
        document.querySelector(showSpan).style.display = "Block";
        document.querySelector(showSpan).innerHTML = `${text} phải từ ${min} đến ${max} ký tự`
        return false;
    }else{
        document.querySelector(showSpan).style.display = "none";
        return true;
    }
};